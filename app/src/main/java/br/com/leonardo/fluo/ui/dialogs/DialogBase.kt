package br.com.leonardo.fluo.ui.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment

abstract class DialogBase : DialogFragment() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent)
	}
}