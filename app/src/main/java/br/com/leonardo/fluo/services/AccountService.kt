package br.com.leonardo.fluo.services

import br.com.leonardo.fluo.models.Account
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface AccountService {
	@POST("account/auth")
	fun auth(@Body account: Account): Call<Account>

	@POST("account")
	fun signup(@Body account: Account): Call<Account>

	@POST("account/forgot")
	fun forgot(@Body account: Account): Call<ResponseBody>

	@Multipart
	@POST("account/photo")
	fun sendPhoto(@Part image: MultipartBody.Part): Call<Account>
}