package br.com.leonardo.fluo.ui.activities

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.DatePicker
import android.widget.TimePicker
import android.widget.Toast
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.app.FluoApp
import br.com.leonardo.fluo.models.Project
import br.com.leonardo.fluo.models.Task
import br.com.leonardo.fluo.services.RetrofitInitializer
import br.com.leonardo.fluo.ui.adapters.AddTaskProjectsAdapter
import kotlinx.android.synthetic.main.activity_add_task.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import android.app.Activity
import br.com.leonardo.fluo.helper.DateTime

class AddTaskActivity : AppCompatActivity(), AddTaskProjectsAdapter.ItemClickListener,
	DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
	private var selectedProject: Project? = null
	lateinit var dateTask: DateTime
	lateinit var adapter: AddTaskProjectsAdapter
	lateinit var projectsList: List<Project>

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_add_task)

		cancelAddTask.setOnClickListener {
			val intent = Intent()
			intent.putExtra("reload", false)
			setResult(Activity.RESULT_OK, intent)
			finish()
		}

		chooseDate.setOnClickListener {
			val now = DateTime.now()

			DatePickerDialog(this, this@AddTaskActivity, now.year, now.month - 1, now.day).show()
		}

		btnAddTask.setOnClickListener {
			var taskTitleVar: String? = null
			var projectId: String? = null

			taskTitle?.let {
				taskTitleVar = it.text.toString()
			}

			this.selectedProject?.let {
				projectId = it.id
			}

			val task = Task()
			task.name = taskTitleVar!!
			task.idProject = projectId!!
			task.idAccountTo = FluoApp.Account!!.id
			task.description = taskTitleVar!!

			val s = RetrofitInitializer().taskService()
			val call = s.addTask(task)

			call.enqueue(object : Callback<ResponseBody> {
				override fun onResponse(
					call: Call<ResponseBody>,
					response: Response<ResponseBody>?
				) {
					response?.let {
						if (it.code() == 201) {
							val intent = Intent()
							intent.putExtra("reload", true)
							setResult(Activity.RESULT_OK, intent)
							finish()
						}
					}
				}

				override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
					Toast.makeText(this@AddTaskActivity, "OPS", Toast.LENGTH_LONG).show()
				}
			})
		}

		getProjetcs()
	}

	private fun getProjetcs() {
		val s = RetrofitInitializer().projectService()
		val call = s.getProjects()

		call.enqueue(object : Callback<List<Project>> {
			override fun onResponse(call: Call<List<Project>>, response: Response<List<Project>>?) {
				response?.let {
					if (it.code() == 200) {
						projectsList = it.body()!!
						configureList()
					}
				}
			}

			override fun onFailure(call: Call<List<Project>>, t: Throwable) {
				Toast.makeText(this@AddTaskActivity, "Ops", Toast.LENGTH_LONG).show()
			}
		})
	}

	private fun configureList() {
		projects.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

		adapter = AddTaskProjectsAdapter(this, this.projectsList, this@AddTaskActivity, -1)

		projects.adapter = adapter
	}

	override fun onItemClick(project: Project, selected: Int) {
		this.selectedProject = project
		projects.adapter =
			AddTaskProjectsAdapter(this, this.projectsList, this@AddTaskActivity, selected)
		projects.scrollToPosition(selected)
	}

	override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
		dateTask = DateTime(year, month, dayOfMonth)
		val now = DateTime.now()

		val timeDialog = TimePickerDialog(
			this,
			this@AddTaskActivity,
			now.hour,
			now.minute,
			true
		)
		timeDialog.show()
	}

	override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
		val now = DateTime.now()

		dateTask.addHour(hourOfDay.toLong())
		dateTask.addMinute(minute.toLong())
		dateTask.addSecond(now.second.toLong())

		var text = ""
		if (dateTask.year == now.year && dateTask.month == now.month - 1 && dateTask.day == now.day) {
			text = getString(R.string.today)
		} else {
			text = String.format(
				"%s/%s/%s",
				dateTask.day.toString().padStart(2, '0'),
				dateTask.month.toString().padStart(2, '0'),
				dateTask.year
			)
		}

		date.text = String.format(
			"%s, %s:%s",
			text,
			hourOfDay.toString().padStart(2, '0'),
			minute.toString().padStart(2, '0')
		)
	}
}
