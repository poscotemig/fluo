package br.com.leonardo.fluo.ui.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import br.com.leonardo.fluo.BuildConfig
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.app.FluoApp
import br.com.leonardo.fluo.helper.SharedPreferencesHelper
import br.com.leonardo.fluo.models.Account
import br.com.leonardo.fluo.services.RetrofitInitializer
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_login)

		btnEntrar.setOnClickListener {
			performLogin()
		}

		btnSignup.setOnClickListener {
			val intent = Intent(this, SignupActivity::class.java)
			startActivity(intent)
		}

		txtForgot.setOnClickListener {
			forgot()
		}

		if (BuildConfig.DEBUG) {
			email.setText("leonardo@leonardo.com")
			password.setText("123123")
		}
	}

	private fun forgot() {
		val account = Account()
		account.email = email.text.toString()

		if (account.email == "") {
			MaterialDialog.Builder(this)
				.theme(Theme.LIGHT)
				.title("Ops")
				.content("Digite seu email")
				.positiveText("Ok")
				.show()

		} else {
			val s = RetrofitInitializer().accountService()
			val call = s.forgot(account)

			call.enqueue(object : retrofit2.Callback<ResponseBody> {
				override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
					response.let {
						if (it!!.code() == 204) {
							Toast.makeText(this@LoginActivity, "Senha enviada para o seu email.", Toast.LENGTH_LONG)
								.show()
						} else {
							MaterialDialog.Builder(this@LoginActivity)
								.theme(Theme.LIGHT)
								.title("Ops")
								.content("Erro. Contate o administrador.")
								.positiveText("Ok")
								.show()
						}
					}
				}

				override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
					MaterialDialog.Builder(this@LoginActivity)
						.theme(Theme.LIGHT)
						.title("Ops")
						.content("Erro. Contate o administrador.")
						.positiveText("Ok")
						.show()
				}
			})
		}
	}

	private fun performLogin() {
		progress.visibility = View.VISIBLE
		val account = Account()
		account.email = email.text.toString()
		account.password = password.text.toString()

		if (account.email == "" || account.password == "") {
			MaterialDialog.Builder(this)
				.theme(Theme.LIGHT)
				.title("Ops")
				.content("Informe email e senha")
				.positiveText("Ok")
				.show()

		} else {
			val s = RetrofitInitializer().accountService()
			val call = s.auth(account)

			call.enqueue(object : retrofit2.Callback<Account> {
				override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
					progress.visibility = View.GONE
					response?.let {
						if (it.code() == 200) {
							FluoApp.Account = it.body()

							SharedPreferencesHelper.saveString(
								this@LoginActivity,
								getString(R.string.FLUO_PREFERENCES),
								getString(R.string.USER_INFO),
								account.getJSON().toString()
							)

							val intent = Intent(this@LoginActivity, MainActivity::class.java)
							startActivity(intent)
							finish()
						} else {
							MaterialDialog.Builder(this@LoginActivity)
								.theme(Theme.LIGHT)
								.title("Ops")
								.content("Usuário ou senha inválidos")
								.positiveText("Ok")
								.show()
						}
					}
				}

				override fun onFailure(call: Call<Account>, t: Throwable) {
					progress.visibility = View.GONE
					MaterialDialog.Builder(this@LoginActivity)
						.theme(Theme.LIGHT)
						.title("Ops")
						.content("Erro. Contate o administrador.")
						.positiveText("Ok")
						.show()
				}
			})
		}
	}
}
