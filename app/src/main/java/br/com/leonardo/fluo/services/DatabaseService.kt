package br.com.leonardo.fluo.services

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface DatabaseService {
	@GET("database")
	fun getDatabase(): Call<ResponseBody>
}