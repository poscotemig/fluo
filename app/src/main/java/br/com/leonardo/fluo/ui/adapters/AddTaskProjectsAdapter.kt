package br.com.leonardo.fluo.ui.adapters

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.models.Project
import kotlinx.android.synthetic.main.item_addtask_projects.view.*

class AddTaskProjectsAdapter(
	val context: Context,
	val lista: List<Project>,
	var listener: ItemClickListener,
	val selected: Int
) : RecyclerView.Adapter<AddTaskProjectsAdapter.AddTaskProjectsViewHolder>() {
	class AddTaskProjectsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		val projectName = itemView.projectName
		val backgroundProject = itemView.backgroundProject

		fun bind(context: Context, project: Project, position: Int, listener: ItemClickListener, selected: Int) {
			projectName.text = project.name

			if (position == selected) {
				backgroundProject.setBackgroundResource(R.drawable.button_green_solid)
				projectName.setTextColor(ContextCompat.getColor(context, R.color.white))
			}

			itemView.setOnClickListener {
				listener.onItemClick(project, position)
			}
		}
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddTaskProjectsViewHolder {
		return AddTaskProjectsViewHolder(
			LayoutInflater.from(context).inflate(
				R.layout.item_addtask_projects,
				parent,
				false
			)
		)
	}

	override fun getItemCount(): Int {
		return lista.size
	}

	override fun onBindViewHolder(holder: AddTaskProjectsViewHolder, position: Int) {
		return holder.bind(context, lista[position], position, listener, selected)
	}

	interface ItemClickListener {
		fun onItemClick(project: Project, selected: Int)
	}
}