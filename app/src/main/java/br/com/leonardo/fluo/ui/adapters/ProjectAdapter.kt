package br.com.leonardo.fluo.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.models.Project
import kotlinx.android.synthetic.main.item_project.view.*

class ProjectAdapter(val context: Context, val list: List<Project>, var listener: ItemClickListener) :
	RecyclerView.Adapter<ProjectAdapter.ProjectViewHolder>() {

	class ProjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		val projectName = itemView.projectName
		val tasks = itemView.numberTasks

		fun bind(item: Project, position: Int, listener: ItemClickListener) {
			projectName.text = item.name
			tasks.text = String.format("%s tasks", item.tasks)

			itemView.setOnClickListener {
				listener.onItemClick(item, position)
			}
		}
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectViewHolder {
		return ProjectViewHolder(LayoutInflater.from(context).inflate(R.layout.item_project, parent, false))
	}

	override fun getItemCount(): Int {
		return list.size
	}

	override fun onBindViewHolder(holder: ProjectViewHolder, position: Int) {
		return holder.bind(list[position], position, listener)
	}

	interface ItemClickListener {
		fun onItemClick(project: Project, position: Int)
	}
}