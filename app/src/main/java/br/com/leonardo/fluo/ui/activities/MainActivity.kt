package br.com.leonardo.fluo.ui.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.Toast
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.app.FluoApp
import br.com.leonardo.fluo.database.fw.DAOBase
import br.com.leonardo.fluo.database.model.TOTask
import br.com.leonardo.fluo.helper.DateTime
import br.com.leonardo.fluo.helper.ImageHelper
import br.com.leonardo.fluo.helper.SDCardUtils
import br.com.leonardo.fluo.helper.SharedPreferencesHelper
import br.com.leonardo.fluo.helper.db.DBHelper
import br.com.leonardo.fluo.models.Account
import br.com.leonardo.fluo.services.RetrofitInitializer
import br.com.leonardo.fluo.ui.fragments.ProjectsFragment
import br.com.leonardo.fluo.ui.fragments.TasksFragment
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {

	private val GALLERY = 1
	private val CAMERA = 2
	private val RELOAD = 3
	private var type: Int = 0
	private var pictureFile: String = ""
	lateinit var imageViewChangeReference: ImageView

//	private val millisInFuture: Long = 60000

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		hello.text = getString(R.string.hello_user, FluoApp.Account!!.name)
		hasTasks.text = getString(R.string.tasks_today, "no")

		Glide
			.with(this)
			.load("https://file.fluo.site/".plus(FluoApp.Account!!.picture))
			.apply(RequestOptions.circleCropTransform())
			.into(userImage)

		setFragment(TasksFragment())

		btnTasks.setOnClickListener {
			projects()
		}

		btnHome.setOnClickListener {
			home()
		}

		btnAddTask.setOnClickListener {
			startActivityForResult(Intent(this, AddTaskActivity::class.java), RELOAD)
		}

		userImage.setOnClickListener {
			photoClick(userImage, 1)
		}

//	    createTimer()

		testeSqlite()
	}

	private fun testeSqlite() {
		val listTasks = DAOBase.list(this, TOTask(), "name")
		Toast.makeText(this, "Quantidade: " + listTasks.size, Toast.LENGTH_LONG).show()

//		val task = TOTask()
//		task.id = DateTime.now().toString("yyyyMMddHHmmss")
//		task.name = "Teste inserção tarefa"
//		task.description = "Teste inserção tarefa sqlite"
//		task.priority = 1f
//		task.createdAt = DateTime.now().millis.toFloat()
//		task.idAccountTo = DateTime.now().toString("yyyyMMddHHmmss")
//		task.idProject = DateTime.now().toString("yyyyMMddHHmmss")
//
//		DAOBase.insert(this, task)
	}

	private fun photoClick(image: ImageView, type: Int) {
		this.type = type
		imageViewChangeReference = image
		showMenuDialog()
	}

	private fun showMenuDialog() {
		Dexter.withActivity(this)
			.withPermissions(
				Manifest.permission.CAMERA,
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.READ_EXTERNAL_STORAGE
			)
			.withListener(object : MultiplePermissionsListener {
				override fun onPermissionsChecked(report: MultiplePermissionsReport) {
					if (report.areAllPermissionsGranted()) {
						showMenu()
					}
				}

				override fun onPermissionRationaleShouldBeShown(
					permissions: MutableList<PermissionRequest>,
					token: PermissionToken
				) {
					token.continuePermissionRequest()
				}
			}).check()
	}

	private fun showMenu() {
		val pictureDialog = android.support.v7.app.AlertDialog.Builder(this)
		pictureDialog.setTitle(getString(R.string.Select))

		val pictureDialogItems = arrayOf(
			getString(R.string.TakeAPicture),
			getString(R.string.PhotoGallery),
			getString(R.string.DownloadDatabase),
			getString(R.string.Exit)
		)

		pictureDialog.setItems(pictureDialogItems) { dialog, which ->
			when (which) {
				0 -> takePhotoFromCamera()
				1 -> choosePhotoFromGallery()
				2 -> downloadDatabase()
				3 -> exitFluo()
			}
		}

		pictureDialog.show()
	}

	private fun takePhotoFromCamera() {
		val image = DateTime.now().toString("yyyyMMddHHmmss")
		val file =
			SDCardUtils.getSdCardFile(this, FluoApp.Directories.IMAGES, image + FluoApp.IMAGE_TYPE)

		pictureFile = file.absolutePath

		val uri = FileProvider.getUriForFile(this, FluoApp.FileProvider.authority, file)

		val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
		i.putExtra(MediaStore.EXTRA_OUTPUT, uri)
		startActivityForResult(i, CAMERA)
	}

	private fun choosePhotoFromGallery() {
		val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
		startActivityForResult(galleryIntent, GALLERY)
	}

	private fun downloadDatabase() {
		val s = RetrofitInitializer().databaseService()
		val call = s.getDatabase()

		call.enqueue(object: Callback<ResponseBody> {
			override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
				response?.let {
					DBHelper.salvarBanco(this@MainActivity, it.body()!!.byteStream())
				}
			}

			override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

			}
		})
	}

	private fun exitFluo() {
		MaterialDialog.Builder(this)
			.theme(Theme.LIGHT)
			.title(R.string.confirm)
			.content(R.string.confirm_exit)
			.positiveText(R.string.confirm_yes)
			.onPositive { dialog, which ->
				SharedPreferencesHelper.delete(
					this,
					getString(R.string.FLUO_PREFERENCES),
					getString(R.string.USER_INFO)
				)

				startActivity(Intent(this, LoginActivity::class.java))
				finish()
			}
			.negativeText(R.string.confirm_no)
			.show()

	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		when (resultCode) {
			Activity.RESULT_OK -> {
				when (requestCode) {
					GALLERY -> {
						data?.let {
							val contentURI = it.data
							try {
								val bitmap =
									MediaStore.Images.Media.getBitmap(contentResolver, contentURI)
								saveBitMapFromGallery(bitmap)
							} catch (e: IOException) {
							}
						}
					}
					CAMERA -> {
						saveBitMapFromCamera()
					}
					RELOAD -> {
						data?.let {
							val reload = it.getBooleanExtra("reload", false)
							if (reload) {
								home()
							}
						}
					}
				}
			}
		}
	}

	private fun saveBitMapFromGallery(bitmap: Bitmap) {
		val bm = ImageHelper.cropToSquare(bitmap)

		val fileName = ImageHelper.imagePath(DateTime.now().toString("yyyyMMddHHmmss")).absolutePath
		val file = ImageHelper.saveImage(this, bm, fileName)

		val myFile = File(file)
		Glide.with(this).load(myFile).apply(RequestOptions.circleCropTransform()).into(userImage)
		sendFile(myFile)
	}

	private fun saveBitMapFromCamera() {
		var bitmap = BitmapFactory.decodeFile(pictureFile)
		val rotate = bitmap.height < bitmap.width
		bitmap = ImageHelper.cropToSquare(bitmap)

		ImageHelper.saveImage(this, bitmap, pictureFile)

		val file = File(pictureFile)
		val uri = Uri.fromFile(file)
		ImageHelper.normalizeImageForUri(this, uri, rotate)

		Glide.with(this).load(file).apply(RequestOptions.circleCropTransform()).into(userImage)
		sendFile(file)
	}

	private fun sendFile(file: File) {
		val s = RetrofitInitializer().accountService()
		s.sendPhoto(
			image = MultipartBody.Part.createFormData(
				"filename",
				file.name,
				RequestBody.create(MediaType.parse("image/*"), file)
			)
		).execute()
	}

	fun setFragment(f: Fragment) {
		val ft = supportFragmentManager.beginTransaction()
		ft.replace(R.id.content, f)
		ft.commit()
	}

	fun setFragmentAndAddToBackstack(f: Fragment) {
		val ft = supportFragmentManager.beginTransaction()
		ft.addToBackStack(null)
		ft.replace(R.id.content, f)
		ft.commit()
	}

	fun home() {
		btnHome.setImageResource(R.drawable.ic_home_active)
		textHome.setTextColor(ContextCompat.getColor(this, R.color.textmenuactive))

		btnTasks.setImageResource(R.drawable.ic_task_inactive)
		textTasks.setTextColor(ContextCompat.getColor(this, R.color.textmenuinactive))

		setFragment(TasksFragment())
	}

	private fun projects() {
		btnHome.setImageResource(R.drawable.ic_home_inactive)
		textHome.setTextColor(ContextCompat.getColor(this, R.color.textmenuinactive))

		btnTasks.setImageResource(R.drawable.ic_task_active)
		textTasks.setTextColor(ContextCompat.getColor(this, R.color.textmenuactive))

		setFragment(ProjectsFragment())
	}
}
