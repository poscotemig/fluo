package br.com.leonardo.fluo.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.helper.SwipeRecyclerView
import br.com.leonardo.fluo.models.Task
import br.com.leonardo.fluo.services.RetrofitInitializer
import br.com.leonardo.fluo.ui.activities.MainActivity
import br.com.leonardo.fluo.ui.adapters.TaskAdapter
import kotlinx.android.synthetic.main.fragment_tasks.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val PROJECT_ID = "PROJECT_ID"

class TasksFragment : Fragment(), SwipeRecyclerView.Companion.SwipeRecyclerViewListener {
	private var projectID: String? = null
	private lateinit var adapter: TaskAdapter
	private var taskList = mutableListOf<Task>()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		arguments?.let {
			projectID = it.getString(PROJECT_ID)
		}
	}

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_tasks, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		getTasks()

		search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
			override fun onQueryTextSubmit(query: String?): Boolean {
				adapter.filter(query!!)

				return false
			}

			override fun onQueryTextChange(newText: String?): Boolean {
				adapter.filter(newText!!)
				return false
			}
		})
	}

	private fun getTasks() {
		this.projectID?.let {
			getProjectTasks()
		} ?: run {
			getUserTasks()
		}

	}

	private fun getProjectTasks() {
		val s = RetrofitInitializer().projectService()
		val call = s.getProjectTasks(this.projectID!!)

		call.enqueue(object : Callback<List<Task>> {
			override fun onResponse(call: Call<List<Task>>, response: Response<List<Task>>?) {
				response?.let {
					if (it.code() == 200) {
						taskList.addAll(it.body()!!)
						configureList()
					}
				}
			}

			override fun onFailure(call: Call<List<Task>>, t: Throwable) {
				Toast.makeText(context, "Ops", Toast.LENGTH_LONG).show()
			}
		})
	}

	private fun getUserTasks() {
		val s = RetrofitInitializer().taskService()
		val call = s.getUserTasks()

		call.enqueue(object : Callback<List<Task>> {
			override fun onResponse(call: Call<List<Task>>, response: Response<List<Task>>?) {
				response?.let {
					if (it.code() == 200) {
						taskList.addAll(it.body()!!)
						configureList()
					}
				}
			}

			override fun onFailure(call: Call<List<Task>>, t: Throwable) {
				Toast.makeText(context, "Ops", Toast.LENGTH_LONG).show()
			}
		})
	}

	private fun configureList() {
		if (this.taskList.size > 0) {
			tasksList.layoutManager = LinearLayoutManager(context)

			adapter = TaskAdapter(context!!, taskList)
			tasksList.adapter = adapter

			SwipeRecyclerView.createSwipe(activity!!, tasksList, this@TasksFragment)
		} else {
			(context as MainActivity).setFragment(NoTaskFragment())
		}
	}

	override fun removeSelectedItem(position: Int) {
		val task = Task()
		task.id = taskList[position].id

		val s = RetrofitInitializer().taskService()
		val call = s.deleteTask(task)

		call.enqueue(object: Callback<ResponseBody> {
			override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>?) {
				response?.let {
					if (it.code() == 202) {
						adapter.removeAt(position)
					}
				}
			}

			override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
				Toast.makeText(context, "Ops", Toast.LENGTH_LONG).show()
			}
		})
	}

	override fun reload() {
		adapter.reload()
	}
}
