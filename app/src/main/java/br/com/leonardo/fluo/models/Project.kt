package br.com.leonardo.fluo.models

data class Project(
	val id: String,
	val name: String,
	val tasks: Int
)