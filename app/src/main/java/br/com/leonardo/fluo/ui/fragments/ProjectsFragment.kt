package br.com.leonardo.fluo.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.models.Project
import br.com.leonardo.fluo.services.RetrofitInitializer
import br.com.leonardo.fluo.ui.activities.MainActivity
import br.com.leonardo.fluo.ui.adapters.ProjectAdapter
import kotlinx.android.synthetic.main.fragment_projects.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val PROJECT_ID = "PROJECT_ID"

class ProjectsFragment : Fragment(), ProjectAdapter.ItemClickListener {

	private var projects = mutableListOf<Project>()

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_projects, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		getProjects()
	}

	private fun getProjects() {
		val s = RetrofitInitializer().projectService()
		val call = s.getProjects()

		call.enqueue(object : Callback<List<Project>> {
			override fun onResponse(call: Call<List<Project>>, response: Response<List<Project>>?) {
				response?.let {
					if (it.code() == 200) {
						projects.addAll(it.body()!!)
						configureGrid()
					}
				}
			}

			override fun onFailure(call: Call<List<Project>>, t: Throwable) {
				Toast.makeText(context, "Ops", Toast.LENGTH_LONG).show()
			}
		})
	}

	private fun configureGrid() {
		projectsList.layoutManager = GridLayoutManager(context!!, 2)

		val adapter = ProjectAdapter(context!!, projects, this@ProjectsFragment)
		projectsList.adapter = adapter
	}

	override fun onItemClick(project: Project, position: Int) {
		val bundle = Bundle()
		bundle.putString(PROJECT_ID, project.id)

		val tasksFragment = TasksFragment()
		tasksFragment.arguments = bundle

		(context as MainActivity).setFragmentAndAddToBackstack(tasksFragment)
	}
}
