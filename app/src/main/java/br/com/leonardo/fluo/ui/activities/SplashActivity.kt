package br.com.leonardo.fluo.ui.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.app.FluoApp
import br.com.leonardo.fluo.helper.SharedPreferencesHelper
import br.com.leonardo.fluo.models.Account
import br.com.leonardo.fluo.services.RetrofitInitializer
import retrofit2.Call
import retrofit2.Response

class SplashActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_splash)

		Handler().postDelayed({
			checkUserAlreadyLoggedIn()
		}, 500)
	}

	private fun checkUserAlreadyLoggedIn() {
		val savedUserInfo = SharedPreferencesHelper.readString(
			this,
			getString(R.string.FLUO_PREFERENCES),
			getString(R.string.USER_INFO)
		)

		savedUserInfo?.let {
			val account = Account()
			account.parseJSON(it)

			performLogin(account)
		} ?: run {
			val intent = Intent(this, LoginActivity::class.java)
			startActivity(intent)
			finish()
		}
	}

	private fun performLogin(account: Account) {
		val s = RetrofitInitializer().accountService()
		val call = s.auth(account)

		call.enqueue(object : retrofit2.Callback<Account> {
			override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
				response.let {
					if (it!!.code() == 200) {
						FluoApp.Account = it.body()

						SharedPreferencesHelper.saveString(
							this@SplashActivity,
							getString(R.string.FLUO_PREFERENCES),
							getString(R.string.USER_INFO),
							account.getJSON().toString()
						)

						val intent = Intent(this@SplashActivity, MainActivity::class.java)
						startActivity(intent)
						finish()
					} else {
						val intent = Intent(this@SplashActivity, LoginActivity::class.java)
						startActivity(intent)
						finish()
					}
				}
			}

			override fun onFailure(call: Call<Account>, t: Throwable) {
				val intent = Intent(this@SplashActivity, LoginActivity::class.java)
				startActivity(intent)
				finish()
			}
		})
	}
}
