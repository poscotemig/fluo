package br.com.leonardo.fluo.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.helper.DateTime
import br.com.leonardo.fluo.models.Task
import kotlinx.android.synthetic.main.item_task.view.*

class TaskAdapter(val context: Context, var list: List<Task>) : RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {
	var listTasks = list as ArrayList<Task>

	class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		val completed = itemView.completed
		val time = itemView.time
		val name = itemView.name
		val alert = itemView.alert

		fun bind(item: Task, position: Int) {
			name.text = item.name

			val date = DateTime(item.createdAt, "dd/MM/yyyy HH:mm:ss")
			time.text = date.toString("hh:mm a")

			when (item.idStatus == "0") {
				true -> {
					completed.setImageResource(R.drawable.ic_unchecked)
				}
				false -> {
					completed.setImageResource(R.drawable.ic_checked)
				}
			}

			when (true) {
				true -> {
					alert.setImageResource(R.drawable.ic_bell)
				}
				false -> {
					alert.setImageResource(R.drawable.ic_grey_bell)
				}
			}
		}
	}

	fun filter(text: String) {
		val listFilter = ArrayList<Task>()

		listTasks.forEach {
			if (it.name.toLowerCase().contains(text.toLowerCase())) {
				listFilter.add(it)
			}
		}

		list = listFilter

		notifyDataSetChanged()
	}

	fun removeAt(position: Int) {
		listTasks.removeAt(position)
		notifyItemRemoved(position)
	}

	fun reload() {
		notifyDataSetChanged()
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
		return TaskViewHolder(LayoutInflater.from(context).inflate(R.layout.item_task, parent, false))
	}

	override fun getItemCount(): Int {
		return list.size
	}

	override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
		return holder.bind(list[position], position)
	}
}