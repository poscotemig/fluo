package br.com.leonardo.fluo.ui.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import br.com.leonardo.fluo.R
import br.com.leonardo.fluo.app.FluoApp
import br.com.leonardo.fluo.helper.SharedPreferencesHelper
import br.com.leonardo.fluo.models.Account
import br.com.leonardo.fluo.services.RetrofitInitializer
import kotlinx.android.synthetic.main.activity_signup.*
import retrofit2.Call
import retrofit2.Response

class SignupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        btnSignup.setOnClickListener {
            this.cadastro()
        }
    }

    private fun cadastro() {
        val account = Account()
        account.name = name.text.toString()
        account.email = email.text.toString()
        account.password = password.text.toString()

        val s = RetrofitInitializer().accountService()
        val call = s.signup(account)

        call.enqueue(object : retrofit2.Callback<Account> {
            override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
                response.let {
                    if (it!!.code() == 200) {
                        performLogin(account)
                    } else {
                        Toast.makeText(this@SignupActivity, "Erro", Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<Account>, t: Throwable) {
                Toast.makeText(this@SignupActivity, "Ops", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun performLogin(account: Account) {
        val s = RetrofitInitializer().accountService()
        val call = s.auth(account)

        call.enqueue(object : retrofit2.Callback<Account> {
            override fun onResponse(call: Call<Account>?, response: Response<Account>?) {
                response.let {
                    if(it!!.code() == 200) {
                        FluoApp.Account = it.body()

                        SharedPreferencesHelper.saveString(this@SignupActivity, getString(R.string.FLUO_PREFERENCES), getString(R.string.USER_INFO), account.getJSON().toString())

                        val intent = Intent(this@SignupActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(this@SignupActivity, "Erro", Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<Account>, t: Throwable) {
                Toast.makeText(this@SignupActivity, "Ops", Toast.LENGTH_LONG).show()
            }
        })
    }
}
