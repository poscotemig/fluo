package br.com.leonardo.fluo.app

import android.app.Application
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.BundleCompat
import br.com.leonardo.fluo.BuildConfig
import br.com.leonardo.fluo.models.Account

class FluoApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    object Directories {
        val IMAGES = "images"
    }

    object FileProvider {
        val authority = "br.com.leonardo.fluo.files.provider.authority.1.x.1.1.abc.xyz_xpna"
    }

    companion object {
        val IMAGE_TYPE = ".png"
        val IMAGE_URL = "https://file.fluo.site/"

        private val PROD = !BuildConfig.DEBUG
        private const val URL_API_DEV = "https://api.fluo.site/v1/"
        private const val URL_API_PROD = "https://api.fluo.site/v1/"
        val URL_API = if (PROD) URL_API_PROD else URL_API_DEV

        val DATABASE_PATH = "/data/data/br.com.leonardo.fluo/databases"
        val DATABASE_NAME = "task.sqlite"
        val ABSOLUTE_DATABASE_PATH = (DATABASE_PATH.plus("/").plus(DATABASE_NAME))
        val DATABASE_PATH_TEMP = "/data/data/br.com.leonardo.fluo/temp"
        val CURRENT_REVISION = 2

        val PATH_APP_TEMP = "/sdcard/fluo/"
        val DATABASE_NAME_TEMP = "task.zip"
        val PATH_APP = "/sdcard/fluo/"
        val ASSETS_DATABASE_PATH = "db"
        val DATABASE_UPDATE = "update.sqlite"

        private var instance: FluoApp? = null

        var Account: Account? = null

        fun getInstance(): FluoApp? {
            return instance
        }
    }
}