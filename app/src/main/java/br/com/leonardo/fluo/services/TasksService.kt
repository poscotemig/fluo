package br.com.leonardo.fluo.services

import br.com.leonardo.fluo.models.Task
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface TasksService {
	@GET("/v1/account/tasks")
	fun getUserTasks(): Call<List<Task>>

	@POST("/v1/task")
	fun addTask(@Body task: Task): Call<ResponseBody>

	@HTTP(method = "DELETE", path = "/v1/task", hasBody = true)
	fun deleteTask(@Body task: Task): Call<ResponseBody>
}