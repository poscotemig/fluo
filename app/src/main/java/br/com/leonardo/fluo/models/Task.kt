package br.com.leonardo.fluo.models

data class Task(
	var id: String = "",
	var idStatus: String = "",
	var name: String = "",
	var idProject: String = "",
	var idAccountTo: String = "",
	var description: String = "",
	var tags: String = "",
	var createdAt: String = ""
)