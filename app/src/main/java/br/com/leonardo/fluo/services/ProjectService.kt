package br.com.leonardo.fluo.services

import br.com.leonardo.fluo.models.Project
import br.com.leonardo.fluo.models.Task
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ProjectService {
    @GET("project")
    fun getProjects() : Call<List<Project>>

    @GET("project/{id}/tasks")
    fun getProjectTasks(@Path("id") projectId: String) : Call<List<Task>>
}