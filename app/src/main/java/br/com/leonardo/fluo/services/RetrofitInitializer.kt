package br.com.leonardo.fluo.services

import br.com.leonardo.fluo.app.FluoApp
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitInitializer {
	companion object {
		private val okHttpClient: OkHttpClient by lazy {
			OkHttpClient.Builder()
				.addInterceptor(HttpLoggingInterceptor().also {
					it.level = HttpLoggingInterceptor.Level.BODY
				})
				.addInterceptor { chain ->
					val newRequest = chain.request().newBuilder()

					FluoApp.Account?.let {
						newRequest.addHeader("token", it.token)
					}

					chain.proceed(newRequest.build())
				}
				.connectTimeout(15, TimeUnit.SECONDS)
				.readTimeout(15, TimeUnit.SECONDS)
				.writeTimeout(15, TimeUnit.SECONDS)
				.build()
		}
	}

	private val retrofit = Retrofit.Builder()
		.client(okHttpClient)
		.baseUrl(FluoApp.URL_API)
		.addConverterFactory(GsonConverterFactory.create())
		.build()

	fun accountService(): AccountService {
		return retrofit.create(AccountService::class.java)
	}

	fun projectService(): ProjectService {
		return retrofit.create(ProjectService::class.java)
	}

	fun taskService(): TasksService {
		return retrofit.create(TasksService::class.java)
	}

	fun databaseService(): DatabaseService {
		return retrofit.create(DatabaseService::class.java)
	}
}